const mysql = require('mysql');

const connection = mysql.createConnection({
   host: 'localhost',
   database: 'todoapp',
   user: 'root',
   password: ''
});

connection.connect((error) => {
    if (error) {
        throw error;
    }
        console.log("MySQL Database is connected successfully");
});

module.exports = connection;