const express = require('express');
const bodyparser = require("express");
const http = require("http");
const cors = require('cors');
const mysql = require("mysql");
const app = express();

app.use(cors());
app.use(express.json());
app.use(bodyparser.json);

const PORT = 3030;
const databaseName = 'todoapp';

const server = http.createServer(app);

const mysqlConnection = mysql.createConnection({
    host: 'localhost',
    database: databaseName,
    user: 'root',
    password: ''
});

mysqlConnection.connect((error) => {
    if (error) {
        console.log("Database error!", error);
        throw error;
    }
    console.log("MySQL Database is connected successfully");
});

const todos = [
    {
        id: 1,
        title: "Go shopping",
        completed: false,
    },
    {
        id: 2,
        title: "Walk dog",
        completed: false,
    }
    ];

app.post("/createTodo", (req, res) => {
    if (!req.body.title) {
        return res.status(400).json({
            error: "Missing todo title"
        });
    }

    const todoTitle = req.body.title;
    const newTodo = {
        id: todos.length,
        completed: false,
        title: todoTitle
    };
    todos.push(newTodo)
    // TODO resime: write into DB

    res.json(newTodo);
});

app.post("/markTodoCompleted/:id", (req, res) => {
    const todoId = Number(req.params.id);
    const todo = todos[todoId];

    if (todo) {
        todo.completed = true;
        res.json(todo);
    } else {
        res.status(404).json({
            error: "Todo does not exist"
        });
    }
});

app.post("/markTodoUncompleted/:id", (req, res) => {
    const todoId = Number(req.params.id);
    const todo = todos[todoId];

    if (todo) {
        todo.completed = false;
        res.json(todo);
    } else {
        res.status(404).json({
            error: "Todo does not exist"
        });
    }
});

app.delete("/deleteTodo/:id", (req, res) => {
    const todoId = Number(req.params.id);
    const todo = todos[todoId];

    if (todo) {
        todos.filter((todo) => todo.id != todoId);
        res.json(todo);
    } else {
        res.status(404).json({
            error: "Todo does not exist"
        });
    }
});

app.get("/getTodo/:id", (req, res) => {
    const todoId = Number(req.params.id);
    const todo = todos[todoId];

    if (todo) {
        res.json(todo);
    } else {
        res.status(404).json({
            error: "Todo does not exist"
        });
    }
});

app.get('/listTodos', (req, res) => {
    // const query1 = "SELECT * from todos";
    // mysqlConnection.query(query1, (err, tasks) => {
    //     if (!err) {
    //         console.log("LIST TODOS: tasks=", tasks);
    //         res.send(tasks);
    //     } else {
    //         console.log(err);
    //     }
    // });

    console.log("todos", todos);
    res.send(todos);
});

server.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});

module.exports = app;
