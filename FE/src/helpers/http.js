import axios from "axios";

const api_url = "http://localhost:3030/";

const client = axios.create({
    baseURL: "http://localhost:3030"
});

const doGet = async (url, params={}) => {
    try {
        let response = await axios({
            method: "get",
            url: api_url + url,
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            params: params
        });
        return response.data;
    } catch (error) {
        return { error };
    }
};

const doPost = async (url, body, responseType) => {
    try {
        let response = await axios({
            method: "post",
            url: api_url + url,
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            data: body,
            responseType
        });
        return response.data;
    } catch (error) {
        return { error };
    }
};


export { doGet, doPost };
