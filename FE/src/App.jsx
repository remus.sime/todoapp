import Navbar from "./Navbar"
import Tasks from "./components/Tasks"
import {Route, Routes } from "react-router-dom"
import {useEffect, useState} from "react";
import {doGet} from "./helpers/http";
import Login from "./pages/Login";

function App() {
    const initialTasks = [
        {id: 0, checked: false, title:"Repair the fridge"},
        {id: 1, checked: true,  title:"Fix the car"},
        {id: 2, checked: true,  title:"Take kids from kindergarden"},
    ];

    const [tasks, setTasks] = useState(initialTasks);
    const [visibleTasks, setVisibleTasks] = useState(initialTasks);

    // useEffect(() => {
    //     getTasks();
    // }, []);
    //
    // const getTasks = () => {
    //     axios({
    //         method:"GET",
    //         url: "http://localhost:3040/listTodos/",
    //     }).then((response) => {
    //         const data = response.data;
    //     }).catch((error) => {
    //         if (error.response) {
    //             console.log(error.response);
    //             console.log(error.response.status);
    //         }
    //     })
    // }

  return (
    <>
      <Navbar />
      <div className="container">
          <Routes>
            <Route path="/" element={
                <Tasks
                    tasks={tasks}
                    setTasks={setTasks}
                    visibleTasks={visibleTasks}
                    setVisibleTasks={setVisibleTasks}
                />
            } />
            <Route path="/about" element={<Login />} />
        </Routes>
      </div>
    </>
  )
}

export default App
