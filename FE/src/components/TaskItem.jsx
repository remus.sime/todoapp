import {
    Checkbox,
    FormGroup,
    FormControlLabel
} from "@mui/material";
import DeleteIcon from "../icons/delete.png";
import {useState} from "react";

export default function TaskItem({ task, tasks, setTasks, visibleTasks, setVisibleTasks }) {
    const [isSelected, setIsSelected] = useState(task.checked);

    const onValueChanged = (e) => {
        setIsSelected((value) => {
            return !value;
        });

        const updatedTasks = tasks.map(currTask => {
            if (currTask.id === task.id) {
                currTask.checked = !currTask.checked;
            }
            return currTask;
        });

        setTasks(updatedTasks);
    };

    const handleDeleteTask = (e) => {
        const taskId = e.target.id;
        setTasks(tasks.filter(task => task.id !== Number(taskId)));
        setVisibleTasks(visibleTasks.filter(task => task.id !== Number(taskId)));
    }

    return (
        <>
            <FormGroup>
                <div>
                    <FormControlLabel
                        className="floatLeft"
                        control={
                            <Checkbox
                                onChange={onValueChanged}
                                checked={isSelected}
                            />}
                        label={task.title}
                    />
                    <span className="floatRight">
                        <img
                            id={task.id}
                            src={DeleteIcon}
                            alt="Delete task"
                            onClick={handleDeleteTask}
                        />
                    </span>
                </div>
            </FormGroup>
        </>
    );
}
