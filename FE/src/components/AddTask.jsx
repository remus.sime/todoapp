import { TextField } from "@mui/material";
import { useState} from "react";

export default function AddTask({ addTask }) {
    const [name, setName] = useState("");

    const handleOnChange = (e) => {
        setName(e.target.value);
    }

    const handleOnEnter = (e) => {
        if (e.key === "Enter") {
            const newTask = {title: name}
            addTask(newTask);
            setName("");
        }
    }

    return (
        <>
            <div>
                <TextField
                    value={name}
                    label="Add a new todo"
                    variant="standard"
                    onChange={handleOnChange}
                    onKeyPress={handleOnEnter}
                />
            </div>
        </>
    );
}
