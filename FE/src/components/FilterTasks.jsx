import React from 'react';
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import RestoreIcon from '@mui/icons-material/Restore';
import Unpublished from '@mui/icons-material/Unpublished';
import TaskAlt from '@mui/icons-material/TaskAlt';

export default function FilterTasks({tasks, setVisibleTasks, setFilteringOption}) {
    const [value, setValue] = React.useState(0);

    const handleFilterTasks = (event, newValue) => {
        setValue(newValue);
        setFilteringOption(newValue);

        if (newValue === 0) {
            setVisibleTasks(tasks);
        } else if (newValue === 1) {
            const completedTasks = tasks.filter(task => task.checked === true);
            setVisibleTasks(completedTasks);
        } else if (newValue === 2) {
            const incompletedTasks = tasks.filter(task => task.checked === false);
            setVisibleTasks(incompletedTasks);
        }
    }

    return (
        <Box sx={{ width: 350 }}>
            <BottomNavigation
                showLabels
                value={value}
                onChange={handleFilterTasks}
            >
                <BottomNavigationAction label="All" icon={<RestoreIcon />} />
                <BottomNavigationAction label="Completed" icon={<TaskAlt />} />
                <BottomNavigationAction label="Incompleted" icon={<Unpublished />} />
            </BottomNavigation>
        </Box>
    );
}

