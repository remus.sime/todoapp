import {doGet, doPost} from "../helpers/http";
import {useEffect, useState} from "react";
import TaskItem from "./TaskItem";
import AddTask from "./AddTask";
import FilterTasks from "./FilterTasks";
import Box from "@mui/material/Box";
import axios from "axios";

export default function Tasks({tasks, setTasks, visibleTasks, setVisibleTasks}) {
    const [filteringOption, setFilteringOption] = useState(0);

    const client = axios.create({
        baseURL: "https://localhost:3040"
    });

    useEffect(() => {
        getTasks();
    }, []);

    const getTasks = () => {
        axios({
            method:"GET",
            url: "http://localhost:3040/listTodos/",
        }).then((response) => {
            const data = response.data;
        }).catch((error) => {
            if (error.response) {
                console.log(error.response);
                console.log(error.response.status);
            }
        })
    }

    const addTask = async (task) => {
        task.id = tasks.length;
        task.checked = false;

        setTasks([...tasks, task]);

        if (filteringOption == 0 || filteringOption == 2) {
            setVisibleTasks([...visibleTasks, task]);
        }
    }

    return (
        <>
            <Box sx={{width: 400}}>
                <AddTask addTask={addTask}></AddTask>

                {visibleTasks.map(task =>
                    <TaskItem
                        key={task.id}
                        task={task}
                        tasks={tasks}
                        setTasks={setTasks}
                        visibleTasks={visibleTasks}
                        setVisibleTasks={setVisibleTasks}
                    />
                )}
                
                <div>
                    <FilterTasks
                        tasks={tasks}
                        setVisibleTasks={setVisibleTasks}
                        setFilteringOption={setFilteringOption}
                    />
                </div>
            </Box>
        </>
    );
}
